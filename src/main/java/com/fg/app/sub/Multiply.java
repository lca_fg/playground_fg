package com.fg.app.sub;

public class Multiply {

	private int m2;
	private int m1;
	
	public Multiply (int m1, int m2) {
		this.m1 = m1;
		this.m2 = m2;
	}
	public int res() {
		return mul(m1,m2);
	}
	private static int mul(int m1, int m2) {
		return m1 * m2;
	}
}
